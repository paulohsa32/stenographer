// Imports the Google Cloud client library
const speech = require("@google-cloud/speech").v1p1beta1;
const fs = require("fs");
const path = require("path");
const multer = require("multer");
const upload = multer({ dest: "tmp/" });
const express = require("express");
const Converter = require("ffmpeg");
const app = express();
const server = require("http").Server(app);

const config = {
  PORT: 3002
};

// Creates a client
const client = new speech.SpeechClient();

const clearDirectory = directory =>
  fs.readdir(directory, (err, files) => {
    if (err) throw err;
    for (const file of files) {
      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
      });
    }
  });

const convertFile = file =>
  new Promise((resolve, reject) => {
    console.log(`Converting ${file.originalname}...`);
    const path = `${__dirname}/tmp/${file.filename}`;
    var process = new Converter(path);

    // TODO: Transform this into a promise so it can work
    return process
      .then(media =>
        media
          .setAudioCodec("flac")
          .setAudioBitRate(128)
          .setAudioFrequency(8000)
          .setAudioChannels(1)
          .save(`${path}.flac`, (error, file) => {
            if (!error)
              console.log(`File converted successfully\nOutput: ${file}`);
            resolve(file);
          })
      )
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });

const transcribeFile = file => {
  console.log(`Starting to transcribe ${file.originalname}`);
  // The name of the audio file to transcribe
  // const path = `${__dirname}/tmp/${file.filename}.flac`;
  const path = file;

  // Reads a local audio file and converts it to base64
  const fileContent = fs.readFileSync(path);
  const audioBytes = fileContent.toString("base64");

  // The audio file's encoding, sample rate in hertz, and BCP-47 language code
  const audio = {
    content: audioBytes
  };
  const config = {
    enableAutomaticPunctuation: true,
    useEnhanced: true,
    encoding: "FLAC",
    languageCode: "en-US",
    sampleRateHertz: 8000,
    model: "phone_call"
  };
  const request = {
    audio,
    config
  };

  console.log("Sending to Google...");
  // Detects speech in the audio file
  return client
    .recognize(request)
    .then(data => {
      const response = data[0];
      const transcription = response.results
        .map(result => result.alternatives[0].transcript)
        .join("\n");
      console.log(`Transcription: ${transcription}`);
      return {
        file: file.originalname,
        transcription
      };
    })
    .catch(err => {
      console.error("ERROR:", err);
    });
};

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use("/", express.static("public/stenographer/dist"));

app.post("/api/transcribe", upload.array("audio", 10), (req, res) => {
  let response = [];
  const promises = [];
  req.files.forEach(file =>
    promises.push(convertFile(file).then(transcribeFile))
  );

  Promise.all(promises).then(data => {
    res.send(data);
    clearDirectory("tmp");
  });
});

server.listen(config.PORT, () => {
  console.log(`Stenographer server running on port ${config.PORT}`);
});
