import * as axios from "axios";

const BASE_URL = "http://localhost:3002";

export const upload = formData => {
  const url = `${BASE_URL}/api/transcribe`;
  return axios.post(url, formData).then(({ data }) => data);
};
